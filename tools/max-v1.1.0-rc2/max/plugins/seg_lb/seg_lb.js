import {Plugin} from '../../../core/ui/js/Plugin.js';

const BREAK_CLASS = "break";
const NO_BREAK_CLASS = "no_break";
const LB_CLASS = "lb";
const HYPHEN_CLASS = "hyphen";


const corrI18n = {
  'fr': {
    'display': 'Sauts de lignes'
  },
  'en': {
    'display': 'Linebreaks'
  }
}

class LbSegPlugin extends Plugin{
  constructor(name) {
    super(name);
  }

  run(){

    console.log("plugin line-beginning running")
    var checkAttr = "";
    if(localStorage.
           getItem(MAX_LOCAL_STORAGE_PREFIX + LB_CLASS) === MAX_VISIBLE_PROPERTY_STRING) 
    {    
      checkAttr = "checked='checked' ";
      //updates visibility
      this.on();
    }
    else{  
      //updates visibility 
      this.off();
     }

     const parser = new DOMParser();
    let node =
      parser.parseFromString(
        "<li><a><input id='toggle_lb' type='checkbox' "
        + checkAttr
        + " name='toggle_lb'>" + corrI18n[lang]['display'] + "</a></li>",
        "text/html")
        .documentElement.querySelector('li');
    document.getElementById('options-list').append(
      document.importNode(node, true)
    );


    document.getElementById('toggle_lb').addEventListener('change', () => {
      this.setLbVisible()
    })
  }
  
  setLbVisible(){
    if (document.getElementById('toggle_lb').checked)
    {
      this.on();
      localStorage.setItem(MAX_LOCAL_STORAGE_PREFIX + LB_CLASS, MAX_VISIBLE_PROPERTY_STRING);
    }
    else{ 
      this.off(); 
      localStorage.removeItem(MAX_LOCAL_STORAGE_PREFIX + LB_CLASS);
    }  
  }
  
  
  /*corr visibles*/
  on(){

    document.querySelectorAll("." +  LB_CLASS).forEach((e) => {
      e.style.display = 'block';
    });
    document.querySelectorAll("." +  HYPHEN_CLASS).forEach((e) => {
      e.style.display = 'inline';
    });
    document.querySelectorAll("." +  BREAK_CLASS).forEach((e) => {
      e.style.display = 'block';
    });
    document.querySelectorAll("." +  NO_BREAK_CLASS).forEach((e) => {
      e.style.display = 'inline';
    });

  }
  /*corr hidden (sic visibles)*/
  off(){

    document.querySelectorAll("." +  LB_CLASS).forEach((e) => {
      e.style.display = 'none';
    });
    document.querySelectorAll("." +  HYPHEN_CLASS).forEach((e) => {
      e.style.display = 'none';
    });
    document.querySelectorAll("." +  BREAK_CLASS).forEach((e) => {
      e.style.display = 'none';
    });
    document.querySelectorAll("." +  NO_BREAK_CLASS).forEach((e) => {
      e.style.display = 'none';
    });

  }
}

MAX.addPlugin(new LbSegPlugin('lbSeg'));
