# MaX

Le Moteur d'Affichage XML est une interface de lecture de sources XML développé par l'[Université de Caen Normandie](http://www.unicaen.fr) ([Pôle Document Numérique](http://www.unicaen.fr/recherche/mrsh/document_numerique) / [CERTIC](https://www.certic.unicaen.fr)) notamment dans le cadre de l'Equipex [Biblissima](http://www.biblissima-condorcet.fr/)

## Licence

voir [legal.txt](legal.txt)

## Participer au développement

Demander à rejoindre [MaX-Community](https://git.unicaen.fr/MaX-Community).

## Contacts

Vous pouvez nous contacter via [contact.certic@unicaen.fr](mailto:contact.certic@unicaen.fr?subject=[MaX])

---

## Prérequis

- Java 11+

- NodeJS (et npm) 10+

- BaseX 10.4+

## Installation

MaX propose un script pour initialiser votre environnement de développement, 
télécharger et installer les dépendances requises.

```bash
$ cd </path/to/max>/tools

# Use global BaseX settings
$ ./max.sh -i

# set the env var $BASEX_PATH only if the basexclient command is not in your PATH. 
# Useless if basex was install with your system package manager
# The basex dir must contains the bin subfolder
$ export BASEX_PATH=</path/to/basex> 
$ ./max.sh -i

# you can also use local BaseX installation without setting any env var
$ ./max.sh -i -b </path/to/basex>

# move to your basex webapp folder 
$ cd </path/to/basex>/webapp
# create a symlink on your MaX instance
$ ln -s </path/to/max> .
# run basex http
$ cd </path/to/basex>/bin
$ ./basexhttp
```
Vous pouvez vérifier votre installation sur http://localhost:8984/max.html

### Éditions de démonstration

```bash
$ cd </path/to/max>/tools

$ ./max.sh --d-tei
# or
$ ./max.sh --d-ead
```

Les éditions de démonstration sont consultables sur :
- http://localhost:8080/max_tei_demo/accueil.html (TEI)
- http://localhost:8080/max_ead_demo/accueil.html (EAD)

## Paramétrage et customisation

La documentation utilisateur est disponible [ici](https://pdn-certic.pages.unicaen.fr/max-documentation/).




![UNICAEN-PDN-CERTIC](https://www.certic.unicaen.fr/ui/images/UNICAEN_PDN_CERTIC.png)

<img src="https://projet.biblissima.fr/sites/default/files/2021-11/biblissima-baseline-sombre-ia.png" alt="Biblissima" width="600px"/>
